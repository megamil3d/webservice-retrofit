package net.megamil.webservicesretrofit;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Criado por Eduardo dos santos em 18/03/2018.
 * Megamil.net
 */

public class Constantes {

    static final OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();

            Request.Builder builder = originalRequest.newBuilder().header("Authorization",
                    Credentials.basic("admin", "40bd001563085fc35165329ea1ff5c5ecbdbbeef"));

            Request newRequest = builder.build();
            return chain.proceed(newRequest);
        }
    }).build();

    static final String servidor = "http://192.168.0.111/prototipo/controller_webservice/";

}

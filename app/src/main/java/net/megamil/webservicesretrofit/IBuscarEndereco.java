package net.megamil.webservicesretrofit;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Criado por Eduardo dos santos em 18/03/2018.
 * Megamil.net
 */

public interface IBuscarEndereco {

    @GET("buscar_cep/cep/{cep}")
    Call<Endereco> getEndereco(@Path("cep") String cep);

    public static final Retrofit retrofit= new Retrofit.Builder()
            .baseUrl(Constantes.servidor)
            .addConverterFactory(GsonConverterFactory.create())
            .build();


}
